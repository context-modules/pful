# Introduction

This is a style and citation module for the [ConTeXt][context] TeX macrolanguage, following as much as possible the (overly complicated) style and citation rules of [Faculty of Law, University of Ljubljana][pful].

# Features

The styling should be perfect for writing a thesis at the Faculty of Law, University of Ljubljana.

It also includes several special citation commands to meet the complicated standards of said faculty.

# How to install

Just download the folder and store it somewhere you can reference it from in your document.

e.g.

	cd my_thesis
	git clone git@gitlab.com:context-modules/pful.git

Tip: It might be a good idea to use Git for your thesis as well and use this module as a [git submodule][sub].

# How to use

In your main ConTeXt document file make sure to include (e.g. in `my_thesis/thesis.tex`):

    \input pful/pful.tex

For more details see `readme.pdf` and `readme.tex` (its source).

[context]: http://wiki.contextgarden.net/Main_Page
[pful]: http://www.pf.uni-lj.si
[sub]: https://git-scm.com/book/en/v2/Git-Tools-Submodules
